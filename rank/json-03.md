---
title: json的简单科普、base64 编码的的应用举例
layout: pages
keywords: [base64 编码, json格式化, json格式验证]
date: 2020-04-15
source: 艾乎网
description: JSON是基于文本的，轻量级的，通常被认为易于读/写。
---

## json的简单科普

JSON（JavaScript Object Notation）是一种基于JavaScript语法子集的开放标准数据交换格式。JSON是基于文本的，轻量级的，通常被认为易于读/写。它与 JavaScript 紧密相连，但  JSON 与语言无关。虽然是独立的，但 JSON 使用与其他语言类似的约定（例如，C，C ++，Java，Perl 和 Python），使 JSON 成为理想的数据交换语言。

关于JSON的特性：
1. 通常在 Web 应用程序开发中使用，JSON 可以用作任何将信息存储为文本的应用程序的数据格式。
2. JSON工作速度快，减少了数据大小并进行了简化文档的处理，广泛应用于web的开发，因此很多人会选择json作为数据交换格式。
3. JSON可以在可能不兼容的技术之间无缝地传输信息。因此，JSON格式验证为大部分人提供了便利条件。

## base64 编码的的应用举例

base64可用于在HTTP环境下传递较长的标识信息。例如，在Java Persistence系统Hibernate中，就采用了Base64来将一个较长的唯一标识符(一般为128-bit的UUID)编码为一个更短的字符串，用作HTTP表单和HTTP GET URL中的参数。

UUID 去掉横行后，可以看作是一个36进制的数字，转换成64进制后，字符串的长度自然更短，用作数据库的主键性能更佳。
