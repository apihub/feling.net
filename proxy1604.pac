var proxy = "SOCKS5 192.168.5.16:7890";

function FindProxyForURL(url, host) {
    if (shExpMatch(host,"*qunar*")
        || shExpMatch(host,"*.cn")
        || shExpMatch(host,"*baidu*")
        || shExpMatch(host,"*bdstatic*")
        || shExpMatch(host,"*aliyun*")
        || shExpMatch(host,"*alicdn*")
        || shExpMatch(host,"*mmstat*")
        || shExpMatch(host,"*alipay*")
        || shExpMatch(host,"*99.com")
        || shExpMatch(host,"*.qq.com")
        || shExpMatch(host,"*.99.com")
        || shExpMatch(host,"101.com")
        || shExpMatch(host,"*.101.com")
        || shExpMatch(host,"*.sdp")
        || shExpMatch(host,"*.nd")
        || shExpMatch(host,"*.ndaeweb.com")
        || shExpMatch(host,"*.hdslb.com")
        ) {
        return "DIRECT"; 
    }

    return proxy; 
}
