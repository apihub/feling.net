// var proxy = "PROXY localhost:7890";
var proxy = "SOCKS5 localhost:7890";

function FindProxyForURL(url, host) {
    if (shExpMatch(host,"*qunar*")
        || shExpMatch(host,"*.cn")
        || shExpMatch(host,"*baidu*")
        || shExpMatch(host,"*bdstatic*")
        || shExpMatch(host,"*aliyun*")
        || shExpMatch(host,"*alicdn*")
        || shExpMatch(host,"*mmstat*")
        || shExpMatch(host,"*alipay*")
        || shExpMatch(host,"*99.com")
        || shExpMatch(host,"*.qq.com")
        || shExpMatch(host,"*.99.com")
        || shExpMatch(host,"101.com")
        || shExpMatch(host,"*.101.com")
        || shExpMatch(host,"*.sdp")
        || shExpMatch(host,"*.nd")
        || shExpMatch(host,"*.ndaeweb.com")
        || shExpMatch(host,"*.hdslb.com")
        ) {
        return "DIRECT"; 
    }

    if (shExpMatch(host,"*google*")
        || shExpMatch(host,"*chrome*")
        || shExpMatch(host,"*gstatic*")
        || shExpMatch(host,"*youtube*")
        || shExpMatch(host,"*ytimg*")
        || shExpMatch(host,"*ggpht*")
        || shExpMatch(host,"*facebook*")
        || shExpMatch(host,"*fbcdn*")
        || shExpMatch(host,"*twitter*")
        || shExpMatch(host,"*twimg*")
        || shExpMatch(host,"*gist*")
        || shExpMatch(host,"*github*")
        || shExpMatch(host,"*wikipedia*")
        || shExpMatch(host,"*stackoverflow*")
        || shExpMatch(host,"*wikimedia*")
        || shExpMatch(host,"*xvideos*")
        || shExpMatch(host,"*porn*")
        || shExpMatch(host,"*.phncdn.com")
        || shExpMatch(host,"*.trafficjunky.net")
        || shExpMatch(host,"*sis001*")
        || shExpMatch(host,"*rubygems*")
        || shExpMatch(host,"*weather*")
        || shExpMatch(host,"*amazonaws*")
        || shExpMatch(host,"*doubleclick.net")
        || shExpMatch(host,"*.doubleclick.net")
        || shExpMatch(host,"*cloudfront.net")
        || shExpMatch(host,"*shadowsocks.com")
        || shExpMatch(host,"*.io")
        || shExpMatch(host,"*.binance.com")
        || shExpMatch(host,"*.bnbstatic.com")
        || shExpMatch(host,"ntotheblock.com")
        || shExpMatch(host,"disqus.com")
        || shExpMatch(host,"*.disqus.com")
        ) {
        return proxy; 
    }

    return "DIRECT"; 
}
