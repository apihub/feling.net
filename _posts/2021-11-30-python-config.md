---
layout: pages
title: python 开发环境 的填坑与备忘
categories: [装机必备]
tags: [python]
---

## 安装多版本

```sh
apt install openssl # 坑 1/4
apt install libssl-dev # 坑 2/4
apt install libffi-dev # 坑 3/4

wget https://www.python.org/ftp/python/3.9.6/Python-3.9.6.tgz
wget https://www.python.org/ftp/python/3.7.11/Python-3.7.11.tgz

extract Python-3.9.6.tgz

cd Python-3.9.6

./configure --with-ssl # 坑 4/4

make

make install

```

## 虚拟环境

```sh
pipenv install --python 3  # 根据 Pipfile 创建虚拟环境（没有 Pipfile 的话就创建空白的虚拟环境）
pipenv shell  # 进入虚拟环境
pipenv --rm  # 删除当前虚拟环境
```


## 修改仓库源

可以用清华或阿里的镜像地址， 替换 Pipfile 中的源地址。

```sh
pip install -i https://pypi.tuna.tsinghua.edu.cn/simple some-package # 临时使用清华源

http://mirrors.aliyun.com/pypi/simple/
```

## 打包、上传 pypi

```sh
pip install twine

pipenv shell

python setup.py install # 本地试安装

rm -r dist && python setup.py bdist_wheel # 打包

twine upload dist/*
```

配置密码，免得每次都要输入

```sh
vi ~/.pypirc

[pypi]
repository: https://upload.pypi.org/legacy/
username: chenyan
password: 密码
```

## mypy 源码错误检查

```sh
pip install mypy
```

vscode 配置

```json
"python.linting.mypyEnabled": true 
```


* 文章目录
{:toc}