---

layout: pages
title: nginx 配置 https
categories: [配置]
lastModifiedDate: 2018-04-06
tags: [ https, certbot, nginx ]

---

## certbot 官网

https://certbot.eff.org/


## 通配符域名
```sh
certbot certonly --manual \
--expand -d "feling.net,*.feling.net" \
--server https://acme-v02.api.letsencrypt.org/directory 
```

只能手动 renew 


* 文章目录
{:toc}