---
layout: pages
title: ssh 配置
categories: [装机必备]
tags: [ssh]
lastModifiedDate: 2021-11-30
---

## 按用户分别配置  

用 `Match` 给特定用户组设置:     

```sh
# fq是希望能使用密码登录的用户组
Match Group fq
    PasswordAuthentication yes
```

## 客户端保持连接

```sh
# 在客户端配置
ServerAliveInterval 60
```

## 允许ipv6

```sh
# inet inet6 any
AddressFamily any
```


## 允许隧道

```sh
PermitTunnel yes
```

>       PermitTunnel
            是否允许 tun(4) 设备转发。可用值如下：
            "yes", "point-to-point"(layer 3), "ethernet"(layer 2), "no"(默认)。
            "yes"同时蕴含着"point-to-point"和"ethernet"。


* 文章目录
{:toc}