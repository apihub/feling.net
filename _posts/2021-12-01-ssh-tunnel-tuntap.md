---
layout: pages
title: ssh 隧道连接两个虚拟网卡
categories: 配置
tags: [ssh, os]
---


## tun/tap 虚拟网卡

linux 内核提供虚拟网卡的功能. 当你创建了一张虚拟网卡, 通过系统调用读取该网卡对应的文件, 就可以读取到链路层或网络层传输的数据. 同样的, 通过系统调用向该文件写入数据, 就相当于通过这张网卡发送数据.

其中, 

tun 设备, 虚拟了网络层设备. 对应的是 IP数据报.

tap 设备, 虚拟了链路层设备, 对应的是 以太网数据帧.

下面这个命令, 可以创建一张能交互链路层数据的虚拟网卡, 设备名称为 tap0, 记住他的设备名称里最后一个数字0, 后面会有用到.

```sh
# 创建一张虚拟网卡, 设备名称为 tap0, 指定为 tap 链路层模式
ip tuntap add tap0 mode tap
```

## ssh -w

ssh 提供现成的命令来连接两张网卡. 在这之前, 确认下 ssh-server 端的配置: [ssh 配置#允许隧道](/pages/2015/05/ssh-config.html#允许隧道)

```sh
ssh -o Tunnel=Ethernet -w 0:0 remotr_user@remote_host
```

`-o Tunnel=Ethernet`: 如果是网络层, 对应的 Tunnel 配置应该是 `-o Tunnel=point-to-point`. 

`-w 0:0`: 这两个0代表的是网卡设备号(如: tap0)结尾的数字. 一个是本地的数字, 一个是远程的数字, 如果记不住前后顺序 `-w local_tun[:remote_tun]`, 可以把两台机器上的网卡取成一样的名字.


## 网络配置

使用 `ssh -w` 命令, 就像是拿根网线把两张网卡接了起来. 接下来还需要手动配置下 ip 和 路由, 网络才能通.


举个例子:
```sh
# 配置机器S的 ip 和 网段 (xxxx:ff46/112)
ifconfig tap0 inet6 add xxxx:ff46/112


# 配置机器C的 ip 和 网段 (xxxx:ff45/122)
ifconfig tap0 inet6 add xxxx:ff45/112
# 配置机器C的 路由规则: 到机器S的数据, 走 tap0 这张网卡)
route -6 add xxxx:ff46/112 dev tap0
```

## ssh 隧道 相关命令
ssh 除了用来远程登录 linux服务器, 还有一系列网络相关的功能, 图形界面转发, 端口转发 等等. 
### ssh -L

把本地的一个端口, 转到远程去. 

```sh
ssh -L local_port:remote_host:remote_port remotr_user@remote_host
```

### ssh -R

把远程的一个端口, 转到本地来. 访问远程机器上的端口时, 就像访问本地端口一样. 通常远程机器是一个有公网ip的云服务器, 这个命令可以当内网穿透使用.

```sh
ssh -R remote_port:local_host:local_port remotr_user@remote_host
```

### ssh -D

把本地的一个端口, 设置为 socks5 代理, 数据将通过远程的机器代理. 

```sh
ssh -D local_port remotr_user@remote_host
```

### 常用参数

`-g`: 相当于把默认的  `Listen 127.0.0.1` 改成 `Listen 0.0.0.0`.

`-q`: 安静模式, 减少 warning 日志的输出.

`-f`: 后台运行.

`-n`: 只能在后台运行时使用, 从 /dev/null 获取输入, 意思是登录后在终端敲的命令都不管用了.

`-N`: 不执行命令 (希望只作端口转发的时候用).

`-C`: 压缩数据, 在慢网络环境下适用.


正确的学习方法: `man ssh`. ssh 隧道这个搜索词, 已经被 `-L` `-R` `-D` 三个参数淹没了, 很难找到介绍 `-w` 参数的文章. 这个时候才能显现出官方文档的好处. 

>
     -q      Quiet mode.  Causes most warning and diagnostic messages to be
             suppressed.
>
     -f      Requests ssh to go to background just before command execution.
             This is useful if ssh is going to ask for passwords or
             passphrases, but the user wants it in the background.  This
             implies -n.  The recommended way to start X11 programs at a
             remote site is with something like ssh -f host xterm.
>
             If the ExitOnForwardFailure configuration option is set to “yes”,
             then a client started with -f will wait for all remote port for‐
             wards to be successfully established before placing itself in the
             background.
>
     -n      Redirects stdin from /dev/null (actually, prevents reading from
             stdin).  This must be used when ssh is run in the background.  A
             common trick is to use this to run X11 programs on a remote
             machine.  For example, ssh -n shadows.cs.hut.fi emacs & will
             start an emacs on shadows.cs.hut.fi, and the X11 connection will
             be automatically forwarded over an encrypted channel.  The ssh
             program will be put in the background.  (This does not work if
             ssh needs to ask for a password or passphrase; see also the -f
             option.)
>
     -N      Do not execute a remote command.  This is useful for just for‐
             warding ports.
>
     -C      Requests compression of all data (including stdin, stdout,
             stderr, and data for forwarded X11, TCP and UNIX-domain connec‐
             tions).  The compression algorithm is the same used by gzip(1),
             and the “level” can be controlled by the CompressionLevel option
             for protocol version 1.  Compression is desirable on modem lines
             and other slow connections, but will only slow down things on
             fast networks.  The default value can be set on a host-by-host
             basis in the configuration files; see the Compression option.




* 文章目录
{:toc}