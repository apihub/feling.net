---
layout: pages
title: android通过USB使用ubuntu的ipv4网络
categories: [配置]
keywords: [安卓, ubuntu, android, 网络共享]
---
再明确下标题: 电脑能上网, 通过USB接口, 共享给没网的手机用.

## 物理连接

手机通过 USB 连接电脑, 并打开 usb 网络共享(或者叫 usb 以太网).

**默认情况下, 共享的方向跟我们想要的是相反的.**

## 查看当前的网络接口情况

在电脑上执行 `ifconfig` 可以看到网络接口的信息.确认下 usb 接口的设备名, 以及能提供互联网接入能力的接口的设备名（一般插网线上网的话会是eth0,  用 wifi上网的话会是 wlan0）    
    
假设: usb 接口名字叫 usb1, 电脑通过 wlan0 接口上网.     


## 配置过程

### 电脑上的配置

1. 切换到 root 用户

2. 启用 ipv4 的转发:     
```sh
echo 1 > /proc/sys/net/ipv4/ip_forward
```
3. 添加 NAT:    
```sh
iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
```
4. 给 usb1 接口分配 ip 地址:    
```sh
ifconfig usb1 192.168.42.121 netmask 255.255.255.0 up
```
另: 查看NAT表项:    
```sh
iptables --table nat -L POSTROUTING
```


### 手机上的设置
1. 通过 adb shell 控制设备    
2. `su` 切换到 root 权限
3. 添加默认网关(电脑上 usb1 的 ip 地址):    
```sh
busybox route add default gw 192.168.42.121
```

## 自动化脚本
按自己的系统环境修改一下变量, 在 root 用户下运行:    

```sh
interface_with_net=wlan0
interface_need_net=usb1
ip_for_interface_need_net=192.168.42.121

echo 1 > /proc/sys/net/ipv4/ip_forward
iptables -t nat -A POSTROUTING -o $interface_with_net -j MASQUERADE
ifconfig $interface_need_net $ip_for_interface_need_net netmask 255.255.255.0 up

adb_path=/home/chenyan/android-sdk-linux/platform-tools/adb
device_name=CB5A1GRUNT
echo busybox route add default gw $ip_for_interface_need_net
$adb_path -s $device_name shell
```

* 文章目录
{:toc}