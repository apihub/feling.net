---

layout: pages
title: zsh 配置
categories: [装机必备]
tags: [zsh]

---
## .zshrc

```sh
plugins=(git extract encode64 urltools sudo)
```



```sh
alias mkdir='mkdir -pv'
alias scp='date && scp'
```


## theme 

```sh
~/.oh-my-zsh/themes/robbyrussell.zsh-theme 
```

```sh
local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ )"
PROMPT='${ret_status}%m %{$fg[cyan]%}%c%{$reset_color%} $(git_prompt_info)'
ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[blue]%}git:(%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[blue]%}) %{$fg[yellow]%}%1{✗%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[blue]%})"
```


* 文章目录
{:toc}