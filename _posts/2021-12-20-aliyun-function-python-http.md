---
layout: pages
title: 用阿里云函数计算提供http服务
categories: [项目]
tags: [python]
published: false
---

想要基于函数计算, 做一个后端服务的脚手架. 


## 

1. controller + service + dao 的分层代码结构
1. mq / http 双入口
1. mysql / mongo 等数据库
1. redis 等中间件

## 

1. 多环境
1. 平滑的发布
1. 发布版本管理
1. 日志收集与统计监控报警
1. 链路追踪


## 函数计算特有的坑与处理方式

### 层管理
第三方库
### 初始化方法
冷启动导致的毛刺


* 文章目录
{:toc}