---

layout: pages
title: git 配置
categories: [装机必备]
tags: [git]

---

## .gitconfig

```
[user]
	name = chenyan
	email = chenyan@feling.net
[alias]
	st = status
	co = checkout
[color]
	diff = auto
	status = auto
	branch = auto
[push]
	default = simple
[core]
    editor = vim
```
